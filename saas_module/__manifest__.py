{
    'name': "SaaS Module",
    'summary': "This is ERISP SAAS Module",
    'description': "This module demonstrates automation of system through software as a service.",
    'author': "javairia",
    'version': "1.0",
    'depends': ['base', 'website'],
    'data': ['views/menu.xml', 
    'security/ir.model.access.csv',
        'views/pricing_page.xml',
        'views/buttons.xml',],
    'installable': True,
    'auto_install': False,

    'qweb': [
       ]
}
