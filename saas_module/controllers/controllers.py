
import base64
import json
import pytz

from datetime import datetime
from psycopg2 import IntegrityError
from werkzeug.exceptions import BadRequest

from flectra import http, SUPERUSER_ID, _
from flectra.http import request
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from flectra.tools.translate import _
from flectra.exceptions import ValidationError, UserError
from flectra.addons.base.models.ir_qweb_fields import nl2br

class controller(http.Controller):
    @http.route('/pricing', auth='public', website=True)
    def hello_page(self, **kwargs):
        return http.request.render('saas_module.pricing_page', {})
