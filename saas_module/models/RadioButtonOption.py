from datetime import date
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra.exceptions import UserError
import logging
    
class RadioButtonOption(models.Model):
    _name = 'radio.button.option'
    _description = 'Radio Button Option'

    name = fields.Char(string='Option Name', required=True)
    value = fields.Char(string='Option Value', required=True)

