from datetime import date
from dateutil.relativedelta import relativedelta

from flectra import api, fields, models, _
from flectra.exceptions import UserError
import logging
    
class HelloModel(models.Model):
    _name = 'hello.model'
    _description = 'Hello Model'

